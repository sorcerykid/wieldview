--------------------------------------------------------
-- Minetest :: WieldView II Mod (wieldview)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2020-2022, Leslie E. Krause
--
-- ./games/minetest_game/mods/wieldview/init.lua
--------------------------------------------------------

local function import( filename )
	local config = { }
	local func = loadfile( filename )
	if func then
		setfenv( func, config )
		local status = pcall( func )
		if not status then
			error( "Syntax error in configuration file: " .. filename )
		end
	end
	return config
end

local mod_path = minetest.get_modpath( "wieldview" )
local config = import( mod_path .. "/config.lua" )
local transforms = dofile( mod_path .. "/transforms.lua" )
local pdata = { }

------------

local function TileReader( def )
	return {
		get_front_texture = function ( )
			local texture = def.tiles[ 6 ] or def.tiles[ 3 ] or def.tiles[ 1 ]
			return texture.name or texture
		end,
		get_left_texture = function ( )
			local texture = def.tiles[ 3 ] or def.tiles[ 1 ]
			return texture.name or texture
		end,
		get_top_texture = function ( )
			return def.tiles[ 1 ].name or def.tiles[ 1 ]
		end,
	}
end

local function get_item_texture( item_name )
	local item_def = minetest.registered_items[ item_name ]
	local texture	

	if not item_def then
		texture = "unknown_item.png"

	elseif item_def.inventory_image ~= "" then
		if transforms[ item_name ] then
			texture = item_def.inventory_image .. "^[transform" .. transforms[ item_name ]
		else
			texture = item_def.inventory_image
		end

	elseif item_def.wield_image ~= "" then
		if transforms[ item_name ] then
			texture = item_def.wield_image .. "^[transform" .. transforms[ item_name ]
		else
			texture = item_def.wield_image
		end

	elseif item_def.tiles then
		local tiles = TileReader( item_def )

		if config.tile_projection == "side" then
			texture = tiles.get_front_texture( )
		elseif config.tile_projection == "top" then
			texture = tiles.get_top_texture( )
		elseif config.tile_projection == "cube" then
			texture = minetest.inventorycube(
				tiles.get_top_texture( ),
				tiles.get_left_texture( ),
				tiles.get_front_texture( )
			)
		end
		texture = texture .. "^[transform" .. config.tile_transform

	else
		texture = "blank.png"

	end

	return texture
end

if not armor and default then
	default.player_register_model( "3d_armor_character.b3d", {
        	animation_speed = 30,
	        textures = {
        	        "character.png",
                	"blank.png",
	                "blank.png",
        	},
	        animations = {
        	        stand = { x = 0, y = 79 },
                	lay = { x = 162, y = 166 },
	                walk = { x = 168, y = 187 },
        	        mine = { x = 189, y = 198 },
                	walk_mine = { x = 200, y = 219 },
	                sit = { x = 81, y = 160 },
        	},
	} )
end

------------

local timer = config.timer_period + config.timer_delay

minetest.register_globalstep( function ( dtime )
	timer = timer - dtime

	if timer <= 0.0 then
		timer = config.timer_period

		for k, v in pairs( pdata ) do
			local item_name =  v.obj:get_wielded_item( ):get_name( )

			if v.item == item_name then return end

			if armor then
				armor.textures[ k ].wielditem =
					item_name ~= "" and get_item_texture( item_name ) or "blank.png"
				armor:update_player_visuals( k )
			elseif default then
			        default.player_set_textures( v.obj, {
			                "character.png",
                			"blank.png",
			                item_name ~= "" and get_item_texture( item_name ) or "blank.png",
			        } )				
			else
				v.obj:set_properties( { textures = {
			                "character.png",
                			"blank.png",
			                item_name ~= "" and get_item_texture( item_name ) or "blank.png",
				} } )
			end

			v.item = item_name
		end
	end
end )

minetest.register_on_joinplayer( function( player )
	pdata[ player:get_player_name( ) ] = { obj = player, item = "" }

	if default and not armor then 
		default.player_set_model( player, "3d_armor_character.b3d" )
	elseif not armor then
		player:set_properties( {
			mesh = "3d_armor_character.b3d",
			textures = { "character.png", "blank.png", "blank.png" },
			visual = "mesh",
			visual_size = { x = 1, y = 1 },
		} )
	end
end )

minetest.register_on_leaveplayer( function( player )
	pdata[ player:get_player_name( ) ] = nil
end )
