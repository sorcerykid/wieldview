WieldView II Mod v1.1
By Leslie E. Krause

WieldView II shows the currently wielded item. It is based on the mod by Stuart Jones, and 
therefore makes use of the player model from 3D Armor for texture mapping.


Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/wieldview

Download archive...
  https://bitbucket.org/sorcerykid/wieldview/get/master.zip
  https://bitbucket.org/sorcerykid/wieldview/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.14+ required

Dependencies
----------------------

Default Mod (optional)
  https://github.com/minetest-game-mods/default

3D Armor Mod (optional)
  https://github.com/stujones11/minetest-3d_armor/tree/master/3d_armor

Installation
----------------------

  1) Unzip the archive into the mods directory of your subgame.
  2) Rename the wieldview master directory to "wieldview".

Source code license
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2020, Leslie E. Krause (leslie@searstower.org)

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html


Multimedia License (textures, sounds, and models)
----------------------------------------------------------

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

   /models/3d_armor_character.b3d
   by Jordach and MirceaKitsune
   obtained from https://github.com/stujones11/minetest-3d_armor/tree/master/3d_armor

You are free to:
Share — copy and redistribute the material in any medium or format.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner, but not in any way
that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that
legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public
domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary
for your intended use. For example, other rights such as publicity, privacy, or moral
rights may limit how you use the material.

For more details:
http://creativecommons.org/licenses/by-sa/3.0/
